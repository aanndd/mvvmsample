//
//  MVVM_ExampleTests.swift
//  MVVM_ExampleTests
//
//  Created by Anand Yadav on 24/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import XCTest
@testable import MVVM_Example


class MVVM_ExampleTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testHasCanada() {
        let viewModel = CanadaViewModel(canada: Canada(title: "Hello", rows: [Row(title: "Hello", rowDescription: "Desc", imageHref: "image.url")]))
        XCTAssert((viewModel.canada != nil))
    }
    
    func testWebService() {
        let promise = expectation(description: "Status code: 200")
        var canada: Canada?
        ServiceManager.getUsers(success: { (response) in
            if let statusCode = response?.response?.httpStatusCode {
                if statusCode == 200 {
                    canada = Canada.map(JSONData:(response?.data)!)!
                    XCTAssertNotNil(canada)
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }) { (error) in
            XCTAssertNil(error)
        }
        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
              XCTFail("error: \(error)")
            }
        }
    }
    
}
