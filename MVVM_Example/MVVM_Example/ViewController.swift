//
//  ViewController.swift
//  MVVM_Example
//
//  Created by Anand Yadav on 24/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var myTableView: UITableView!
    var viewModel: CanadaViewModel?
    
    // required when called from storyboard
    required init?(coder aDecoder: NSCoder) {
        viewModel = CanadaViewModel()
        super.init(coder: aDecoder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myTableView.tableFooterView = UIView()

        viewModel?.fetchData { (canada, error) in
            if canada != nil {
                DispatchQueue.main.async {
                    self.title = canada?.title
                    self.myTableView.reloadData()
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.canada?.rows?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! MyTableViewCell
        let rowdata: Row = (viewModel?.canada?.rows![indexPath.row])!
        cell.lblTitle.text = rowdata.title
        cell.lblDetailsText.text = rowdata.rowDescription
        if let imageURL = rowdata.imageHref {
            cell.modelImageView.loadImageUsingCache(withUrl: imageURL)
        }
        return cell
    }
}

extension UIImageView {
    static let imageCache = NSCache<NSString, UIImage>()

    func loadImageUsingCache(withUrl urlString : String) {
        let url = URL(string: urlString)
        if url == nil {return}
        self.image = nil

        // check cached image
        if let cachedImage = UIImageView.imageCache.object(forKey: urlString as NSString)  {
            self.image = cachedImage
            return
        }

        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
        addSubview(activityIndicator)
        activityIndicator.startAnimating()
        activityIndicator.center = self.center

        // if not, download image from url
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }

            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    UIImageView.imageCache.setObject(image, forKey: urlString as NSString)
                    self.image = image
                    activityIndicator.removeFromSuperview()
                }
            }

        }).resume()
    }
}
