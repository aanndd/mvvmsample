//
//  ServiceHelper.swift
//  JsonParser
//
//  Created by Anand Yadav on 06/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit
let baseURL = URL(string: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl")!

enum ServiceHelper
{
    case getAlbum
    case getUsers
    case getPosts
    case getComments
    case getPhotos
    

    var method: HTTPMethod {
        switch self {
        case .getAlbum:
            return .get
        case .getComments:
            return .get
        case .getPosts:
            return .get
        case .getUsers:
            return .get
        case .getPhotos:
            return .get
        }
    }

    var path: String {
        switch self {
        case .getAlbum:
            return "/facts"
        case .getComments:
            return "/comments"
        case .getUsers:
            return "/facts"
        case .getPosts:
            return "/posts"
        case .getPhotos:
            return "/photos"
        }
    }
        
    func asURLRequest() -> URLRequest {
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")

        print(urlRequest)
        return urlRequest
    }
}

public enum HTTPMethod : String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}

struct Response {
    var response: URLResponse?
    var httpStatusCode: Int = 0
    //var headers = RestEntity()

    init(fromURLResponse response: URLResponse?) {
        guard let response = response else { return }
        self.response = response
        httpStatusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
        
//        if let headerFields = (response as? HTTPURLResponse)?.allHeaderFields {
//            for (key, value) in headerFields {
//                headers.add(value: "\(value)", forKey: "\(key)")
//            }
//        }
    }
}

struct Results {
    var data: Data?
    var response: Response?
    init(withData data: Data?, response: Response?) {
        self.data = data
        self.response = response
    }
}

extension Decodable {
    static func map(JSONData:Data) -> Self? {
        debugPrint(JSONData)

        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let utf8Data = String(decoding: JSONData, as: UTF8.self).data(using: .utf8)
            return try decoder.decode(Self.self, from: utf8Data!)
        } catch let error {
            print(error)
            return nil
        }
    }
}
