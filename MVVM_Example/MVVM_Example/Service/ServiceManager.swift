//
//  ServiceManager.swift
//  MVVM_Example
//
//  Created by Anand Yadav on 24/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ServiceManager: NSObject {
    static func getUsers(success: @escaping (_ response: Results?) -> Void,
                         failure: @escaping (_ error: Error?) -> Void) {
        BaseService.sharedInstance.apiRequest(urlRequest: ServiceHelper.getUsers.asURLRequest(), success: { (results) in
            success(results)
        }) { (error) in
            failure(error)
        }
    }
}
