//
//  CanadaViewModel.swift
//  MVVM_Example
//
//  Created by Anand Yadav on 24/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

protocol CanadaViewModelEvents: class {
    func fetchData(completion: @escaping (Canada?, Error?) -> Void)
}

class CanadaViewModel: CanadaViewModelEvents {
    
    var delegate : CanadaViewModelEvents?
    var canada: Canada?
    
    init(canada: Canada? = nil) {
        self.canada = canada
    }
    
    init(delegate: CanadaViewModelEvents) {
        self.delegate = delegate
    }
    
    func fetchData(completion: @escaping (Canada?, Error?) -> Void) {
        ServiceManager.getUsers(success: { (response) in
            self.canada = Canada.map(JSONData:(response?.data)!)!
            completion(self.canada, nil)
            print(self.canada as Any)
        }) { (error) in
            completion(nil, error)
        }
    }
    
}
